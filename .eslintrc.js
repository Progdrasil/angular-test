/**
 * ESLINT rules
 * @author Félix Brunet
 * @name nodeDevelopment
 * @version 1.0.0
 */
module.exports = {
    "env": {
        "node" : true,
        "es6": true,
    },
    extends: [
        "eslint:recommended",
        "plugin:@typescript-eslint/eslint-recommended",
        "plugin:@typescript-eslint/recommended"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2018,
        sourceType : "module"
    },
    parser: "@typescript-eslint/parser",
    plugins: ["@typescript-eslint"],
    "overrides": [
        {
            "files": [ "test/**/*.test.ts", "**/*.spec.ts", "e2e/**/*.ts" ],
            "env": {
                "jasmine": true
            },
            "rules": {
                "no-restricted-syntax" : ["error", //remove the restriction on null
                    "WithStatement",
                    "ForOfStatement",
                    "ForInStatement",
                    "ForStatement",
                    "SwitchStatement",
                    "SwitchCase",
                ],
                "@typescript-eslint/no-explicit-any": "off"
            }
        }, {
            "files": [ "**/*.conf.js"],
            "env": {
                "jasmine": true
            },
            "rules": {
                "@typescript-eslint/no-var-requires": "off"
            }
        }
    ],
    "rules": {
        "@typescript-eslint/explicit-function-return-type" : "off",
        "@typescript-eslint/camelcase": "off",
        "@typescript-eslint/no-empty-function" : ["error", { "allow" : ["constructors"] }],
        "@typescript-eslint/no-explicit-any" : ["error"],
        "@typescript-eslint/interface-name-prefix" : "off",
        "@typescript-eslint/no-unused-vars": ["error"],
        "@typescript-eslint/no-use-before-define" : ["error", "nofunc"],
        "indent": [
            "error",
            4
        ],
        "quotes": [
            "error",
            "double",
            "avoid-escape"
        ],
        "semi": [
            "error",
            "always"
        ],
        "dot-location" : ["error", "property"],
        "no-unused-vars": "off", // Typescript above is more exhaustive due to typing
        "no-console" : "off",
        "no-inner-declarations" : "off",
        "consistent-return": "error",
        "no-eval" : "error",
        "no-eq-null" : "error", //=== null only
        "eqeqeq" : "error",
        "no-new" : "error", //only in assign
        "no-proto" : "error",
        "no-return-assign" : "error",
        "no-return-await" : "error",
        "no-with" : "error",
        "no-use-before-define" : ["error", "nofunc"],
        //style,
        // "camelcase" : "error",
        "comma-spacing" : "error",
        "func-call-spacing" : "error",
        //"key-spacing" : ["error", {"beforeColon" : true}], //I don't care for beforeColon...
        "max-len" : ["error", {
            "code" : 125,
            "ignoreComments" : true,
            "ignoreUrls" : true,
            "ignoreStrings" : true,
            "ignoreTemplateLiterals" : true,
            "ignoreRegExpLiterals" : true
        }],
        "max-lines" : ["error", {
            "max" : 300,
            "skipComments" : true
        }],
        "max-nested-callbacks" : ["error", 7],
        "no-array-constructor" : "error",
        "no-bitwise" : ["error", { "allow": ["~"] }],
        "no-continue" : "error",
        "no-lonely-if" : "error",
        "no-mixed-operators" : "error",
        "no-multi-assign" : "error",
        "no-multiple-empty-lines" : "error",
        "no-new-object" : "error",
        "no-restricted-syntax" : ["error",
            "WithStatement", //with
            "ForOfStatement", //remove for loop.
            "ForInStatement", //remove for in, for of -> use Array.forEach
            "ForStatement",
            "SwitchStatement", //remove switch -> use object/Map instead.
            "SwitchCase",
            {
                "selector": ":not(BinaryExpression:matches([operator='!=='], [operator='==='])) > Literal[value=null]",
                "message": "Usage of \"null\" is deprecated except when received from legacy APIs; use \"undefined\" instead"
            }
        ],
        "no-trailing-spaces" : "error",
        "no-unneeded-ternary" : ["error", { "defaultAssignment" : false }],
        "no-whitespace-before-property": "error",
        "nonblock-statement-body-position" : "error",
        "object-curly-newline" : "error",
        "object-curly-spacing" : ["error", "always"], // lets the code breath a bit
        "one-var" : ["error", "never"],
        "operator-linebreak" : ["error", "after"],
        "space-before-blocks" : "error",
        "space-before-function-paren" : ["error", {
            "anonymous": "never",
            "named": "never",
            "asyncArrow": "always"
        }],
        "template-tag-spacing" : "error",
        "no-duplicate-imports" : "error",
        "no-useless-computed-key" : "error",
        "no-useless-rename" : "error",
        "no-var" : "error",
        "prefer-const" : ["error", { "ignoreReadBeforeAssign" : false }],
        "prefer-template" : "error",
        "template-curly-spacing" : "error",
        "eol-last": ["error", "always"],
    }
};
